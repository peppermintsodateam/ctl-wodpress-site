<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ctlconsu_ctlconsult');

/** MySQL database username */
define('DB_USER', 'ctlconsu_ctluser');

/** MySQL database password */
define('DB_PASSWORD', 'aT#?~8g~EOIX');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y~OyP>]x<8+7sxj7&d*}en8(?P9GRsW_ yTG6e2ju&R)anBF/-<uX*=+G!?%}^jp');
define('SECURE_AUTH_KEY',  '`8O`a60o?96[G&S%e<W;.GYBR-p+ZF:.S,e@gg%pbQ?-FbP9rpfp0-cm|J@5vI(A');
define('LOGGED_IN_KEY',    '*eCwR:$4uUAjGyJUd;MG2it2!FsnZCYIQbfz+.oypEZNf/cl;/{ZHKHE;?NPxZUL');
define('NONCE_KEY',        '4<j}^Lu(s}/#5~#dros-F^vK+ j~<E!z|GKL1[n<%c2[u$1p@,uG?(A+C_6Y>XdH');
define('AUTH_SALT',        ';PaVxrJK/v_&G2M{P9Zi7P&;NfymscLCOX,h-m,k6[&c?X3S|})bbv</GoA8pHbV');
define('SECURE_AUTH_SALT', '9.YzuXvM?zB=WOs@ghN[#D@D**L}A.gT: 8&]YGPR4|rX67f@hKRC+5lZo1TG%i!');
define('LOGGED_IN_SALT',   'Lvv+!WURpkPv<gk#JD^<YD Uk7o^Dv.d-]vy&+T)Af}xn9OSb[B>L_D#MamO?C_v');
define('NONCE_SALT',       '45EA?$ ~boM+4z6v++b%j djj:}x5-]LY 6/YwQ>]1q$`jc[Wv{S|=r[7Y2*+,c0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
