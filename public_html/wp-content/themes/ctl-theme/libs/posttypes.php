<?php

	add_action('init', 'ctl_init_posttypes');

	function ctl_init_posttypes(){

		/*Team posttypes*/
		$labels = array(
			'name'=>__('Team'),
			'singular_name'=>__('team'),
			'add_new'=>__('Add new team'),
			'add_new_item'=>__('Add new team'),
			'edit_item'=>__('Edit team'),
			'new_item'=>__('New team'),
			'view_item'=>__('View team'),
			'search_items'=>__('Search team'),
			'not_found'=>__('No featured team'),
			'not_found_in_trash' =>__('No team found in trash')
		);

		$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'comments',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
			'taxonomies'=>array('team_category', 'team_tag'),
		);

		register_post_type('team', $args);



		/*News posttypes*/
		$labels = array(
			'name'=>__('News'),
			'singular_name'=>__('news'),
			'add_new'=>__('Add new news'),
			'add_new_item'=>__('Add new news'),
			'edit_item'=>__('Edit news'),
			'new_item'=>__('New news'),
			'view_item'=>__('View news'),
			'search_items'=>__('Search news'),
			'not_found'=>__('No featured news'),
			'not_found_in_trash' =>__('No news found in trash')
		);

		$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'comments',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
			'taxonomies'=>array('news_category', 'news_tag'),
		);

		register_post_type('news', $args);



		/*Events posttypes*/
		$labels = array(
			'name'=>__('Events'),
			'singular_name'=>__('Events'),
			'add_new'=>__('Add new event'),
			'add_new_item'=>__('Add new event'),
			'edit_item'=>__('Edit events'),
			'new_item'=>__('New events'),
			'view_item'=>__('View events'),
			'search_items'=>__('Search events'),
			'not_found'=>__('No featured events'),
			'not_found_in_trash' =>__('No news found in events')
		);

		$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'comments',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
			'taxonomies'=>array('events_category', 'events_tag'),
		);

		register_post_type('events', $args);




		/*Testimonials posttypes*/

		$labels = array(
			'name'=>__('Testimonials'),
			'singular_name'=>__('testimonials'),
			'add_new'=>__('Add new testimonial'),
			'add_new_item'=>__('Add new testimonial'),
			'edit_item'=>__('Edit testimonials'),
			'new_item'=>__('New testimonial'),
			'view_item'=>__('View testimonials'),
			'search_items'=>__('Search testimonials'),
			'not_found'=>__('No featured testimonials'),
			'not_found_in_trash' =>__('No testimonials found in trash')
		);

		$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'comments',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
			'taxonomies'=>array('testimonials_category', 'testimonials_tag'),
		);

		register_post_type('testimonials', $args);




		/*Clients posttypes*/
		$labels = array(
			'name'=>__('Clients'),
			'singular_name'=>__('clients'),
			'add_new'=>__('Add new clients'),
			'add_new_item'=>__('Add new clients'),
			'edit_item'=>__('Edit clients'),
			'new_item'=>__('New clients'),
			'view_item'=>__('View clients'),
			'search_items'=>__('Search clients'),
			'not_found'=>__('No featured clients'),
			'not_found_in_trash' =>__('No clients found in trash')
		);



		$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'comments',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
			'taxonomies'=>array('clients_category', 'clients_tag'),
		);

		register_post_type('clients', $args);


	}// End posttypes function.



	//TAXONOMIES
	add_action('init','ctl_init_taxonomies');

	function ctl_init_taxonomies() {


	
		/*Taxonomy location*/
		register_taxonomy('team_category', 'team',
			array(
				'labels'=>array(
					'name' => 'Categories',
					'singular_name'=>'Category',
					'search_items'=>'Search Categories',
					'all_items'=>'All Categories',
					'edit_item'=>'Edit Category',
					'update_item'=>'Update Category',
					'add_new_item'=>'Add New Category',
					'new_item_name'=>'New Category Name',
					'menu_name'=>'Categories'
				),

				'hierarchical'=>true,
				'sort'=>true,
				'args'=>array('orderby'=>'term_order'),
				'rewrite'=> true,
				'show_admin_column'=>true
			)

		);


		/*Taxonomy clients*/
		register_taxonomy('clients_category', 'clients',
			array(
				'labels'=>array(
					'name' => 'Categories',
					'singular_name'=>'Category',
					'search_items'=>'Search Categories',
					'all_items'=>'All Categories',
					'edit_item'=>'Edit Category',
					'update_item'=>'Update Category',
					'add_new_item'=>'Add New Category',
					'new_item_name'=>'New Category Name',
					'menu_name'=>'Categories'
				),

				'hierarchical'=>true,
				'sort'=>true,
				'args'=>array('orderby'=>'term_order'),
				'rewrite'=> true,
				'show_admin_column'=>true
			)

		);


		/*Taxonomy news*/
		register_taxonomy('news_category', 'news',
			array(
				'labels'=>array(
					'name' => 'Categories',
					'singular_name'=>'Category',
					'search_items'=>'Search Categories',
					'all_items'=>'All Categories',
					'edit_item'=>'Edit Category',
					'update_item'=>'Update Category',
					'add_new_item'=>'Add New Category',
					'new_item_name'=>'New Category Name',
					'menu_name'=>'Categories'
				),

				'hierarchical'=>true,
				'sort'=>true,
				'args'=>array('orderby'=>'term_order'),
				'rewrite'=> true,
				'show_admin_column'=>true
			)

		);


		/*Taxonomy events*/
		register_taxonomy('events_category', 'events',
			array(
				'labels'=>array(
					'name' => 'Categories',
					'singular_name'=>'Category',
					'search_items'=>'Search Categories',
					'all_items'=>'All Categories',
					'edit_item'=>'Edit Category',
					'update_item'=>'Update Category',
					'add_new_item'=>'Add New Category',
					'new_item_name'=>'New Category Name',
					'menu_name'=>'Categories'
				),

				'hierarchical'=>true,
				'sort'=>true,
				'args'=>array('orderby'=>'term_order'),
				'rewrite'=> true,
				'show_admin_column'=>true
			)

		);


		/*News Tag - Taxonomy*/

		register_taxonomy('news_tag','news',

		array(
			'labels'=>array(
				'name' => 'Tags',
				'singular_name'=>'Tag',
				'search_items'=>'Search Tags',
				'all_items'=>'All Tags',
				'edit_item'=>'Edit Tag',
				'update_item'=>'Update Tag',
				'add_new_item'=>'Add New Tag',
				'new_item_name'=>'New Tag Name',
				'menu_name'=>'Tags'
			),
			'hierarchical'=>true,
			'sort'=>true,
			'args'=>array('orderby'=>'term_order'),
			'rewrite'=> true,
			'show_admin_column'=>true
		)
	);


		/*Events Tag - Taxonomy*/

		register_taxonomy('events_tag','events',

		array(
			'labels'=>array(
				'name' => 'Tags',
				'singular_name'=>'Tag',
				'search_items'=>'Search Tags',
				'all_items'=>'All Tags',
				'edit_item'=>'Edit Tag',
				'update_item'=>'Update Tag',
				'add_new_item'=>'Add New Tag',
				'new_item_name'=>'New Tag Name',
				'menu_name'=>'Tags'
			),
			'hierarchical'=>true,
			'sort'=>true,
			'args'=>array('orderby'=>'term_order'),
			'rewrite'=> true,
			'show_admin_column'=>true
		)
	);



	}


 ?>