<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes('html'); ?>> <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
       <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
        <meta content="yes" name="apple-mobile-web-app-capable">
        <meta name="msapplication-tap-highlight" content="no" />


        <link rel="stylesheet" href="<?php echo THEME_PATH; ?>/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo THEME_PATH; ?>/css/bootstrap-theme.min.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Oswald:300,400,700" rel="stylesheet"> 
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo THEME_PATH; ?>/css/normalize.css">
        <link rel="stylesheet" href="<?php echo THEME_PATH; ?>/css/animations.css">
        <link rel="stylesheet" href="<?php echo THEME_PATH; ?>/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo THEME_PATH; ?>/css/main.css">

        <script src="<?php echo THEME_PATH; ?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


        <?php if(is_search()) :  ?>
          <meta name="robots" content="noindex, nofollow" />
        <?php endif; ?>

        <?php if(is_front_page() ) : ?>
          <title><?php bloginfo('sitename'); ?></title>
           <?php else : ?>
          <title><?php wp_title(''); ?></title>
        <?php endif; ?>

        <script src="https://use.typekit.net/lso0mbo.js"></script>
         <script>
          window.onload = function () {
            try {
              Typekit.load();
            } catch(e) {}
          };
      </script>

        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" >

        <div id="wptime-plugin-preloader"></div>
        <?php wp_head(); ?>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-18056331-1', 'auto');
            ga('send', 'pageview');

        </script>

    </head>


    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


        <!-- MAIN NAVIGATION -->

         <?php get_template_part('templates/content', 'menu') ?>

        <!-- END OF MAIN NAVIGATION -->

        <header id="main-header">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">

                <div class="nav-panel clearfix">
                  <div class="logo pull-left"><a title="Return to the homepage" href="<?php echo esc_url(home_url('/')) ?>"><img class="img-responsive" src="<?php echo THEME_PATH; ?>/img/main-logo.png" alt="CTL Consult Logo"></a></div>

                  <div class="switcher-wrapper">
                    <div id="mob-nav">
                      <span class="slice1"></span>
                      <span class="slice2"></span>
                      <span class="slice3"></span>
                    </div>
                  </div>
                </div>

                
              </div>
            </div>
          </div>
        </header>