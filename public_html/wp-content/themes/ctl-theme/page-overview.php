<?php get_header(); 

/*
Template Name: Overview Template
*/
?>

<div class="main-container">

	<section id="how-we-do-it-overview">
		<div class="container ajax-page-content">

		<div class="row ajax-row">

		<div class="col-xs-12 col-md-5 cell">
			<figure class="hero-wrapper change-size">
                <img src="<?php echo THEME_PATH; ?>/img/how-we-do-it.png" class="img-responsive" alt="how-we-do-it">
            </figure>
		</div>

			<div class="col-xs-12 col-md-7 cell">
				<?php if(have_rows('section')): ?>
                     <?php while(have_rows('section')):the_row();  ?>
                        <section class="section-overview">
                        	<?php if(have_rows('section_inside')): ?>
								<?php while(have_rows('section_inside')):the_row();  ?>
									<h4 class="inner-overview-header"><?php the_sub_field('header'); ?></h4>
									<?php the_sub_field('text_inside'); ?>
								<?php endwhile; ?>
                        		<?php endif; ?>
                        </section>        
                  	<?php endwhile; ?>
              <?php endif; ?>
			</div>

		</div>

	</div>
	</section>


</div>

<?php get_footer(); ?>