<div class="topic-content-inside padding-topic">
                 <div class="container">
                   <div class="row">
                     <div class="col-xs-12 col-md-8 col-md-offset-2">
                       <figure class="banner-spec">
                          <img src="<?php echo THEME_PATH; ?>/img/banner-specialists.png" alt="banner-specialists" class="img-responsive">
                       </figure>
                     </div>

                     <div class="col-xs-12">

                          <?php 
                            $args = array(
                                'post_type'=>'team',
                                'posts_per_page'=> -1
                              );
                            $team_loop = new WP_Query($args);
                            if($team_loop->have_posts() ) : 
                           ?>

                       <div class="team-rotator clearfix">
                          
                            <?php while($team_loop->have_posts()) : $team_loop->the_post(); ?>
                              <div class="owl-item">
                                <div class="team-box">
                                  <figure>
                                    <?php if ( has_post_thumbnail() ) : ?>
                                        <?php the_post_thumbnail(); ?>
                                      <?php endif; ?>
                                  </figure>
                                   <h4 class="team-header"><?php the_title(); ?></h4>
                                   <?php the_content(); ?>
                                </div>
                              </div>
                            <?php endwhile; ?>
                       </div>
                       
                       <?php endif; ?>

                     </div>
                   </div>
                 </div>
               </div>