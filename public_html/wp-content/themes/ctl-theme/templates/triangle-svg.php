<div class="bottom-elements bottom-shape">
            <svg version="1.1" id="triangle-svg" class="tr-shape" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" width="100%" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 2190.1 265.3" style="enable-background:new 0 0 2190.1 265.3;" xml:space="preserve">
            <style type="text/css">
              .st0{fill:#FDCA09;}
              .st1{fill:#8E3A95;}
              .st2{fill:#EC1066;}
              .st3{fill:#AACF37;}
              .st4{fill:#70CEEA;}
              .st5{fill:#FFFFFF;stroke:#000000;stroke-miterlimit:10;}
            </style>
        <g>
          <polygon class="st0" points="437.8,265.3 1093.6,0 1093.6,0 1092.9,0 0,265.3   "/>
          <polygon class="st1" points="875.4,265.3 1095,-0.1 1095,-0.8 1094,-0.8 1094,0 436,265.3   "/>
          <polygon class="st2" points="1313.5,265.3 1096,0 1096,0 1094.8,0 872.7,265.3  "/>
          <polygon class="st3" points="1752.2,265.3 1096.8,0 1095.8,0 1095.8,0 1313.4,265.3   "/>
          <polygon class="st4" points="2190.1,265.3 1097.7,0 1096.8,0 1752.2,265.3  "/>
        </g>
       
      </svg>
  </div>