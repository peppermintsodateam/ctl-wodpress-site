<aside id="sidebar" class="offCanvas">
           <div class="menu-inner">
             <nav>

              <?php wp_nav_menu(array(
                'theme_location'=>'main-menu',
                'menu_class'=>'main-nav'
              ));
            ?>

                <ul class="second-nav">
                  <li><a href="#contact-panel">Contact</a></li>
                </ul>
             </nav>
             <a href="#" class="close-menu"></a>
           </div>
         </aside>