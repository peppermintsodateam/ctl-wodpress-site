<div class="contact-us-panel offBottom">
  <div class="inner-panel-contact">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
              <?php if(is_active_sidebar('footer-sidebar')) : ?>
                <?php dynamic_sidebar('footer-sidebar'); ?>
            <?php endif; ?>
            </div>

          </div>
          <div class="row">

            <div class="col-xs-12 col-md-8 col-md-offset-2 form-wrapper">

              <?php echo do_shortcode('[contact-form-7 id="177" title="Bottom contact form" html_id="contact-form-ctl"]') ?>

            </div>
          </div>

          



      </div>
  </div>
  
      
      <a href="#" class="close-menu close-contact"></a>
    </div>