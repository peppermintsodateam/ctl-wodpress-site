<aside id="sidebar" class="offCanvas nav-index">
           <div class="menu-inner">
             <nav>
               <ul class="main-nav">
                  <?php generate_menu(); ?> 
                </ul>

                <ul class="second-nav">
                  <li><a href="#contact-panel">Contact</a></li>
                </ul>
             </nav>
             <a href="#" class="close-menu"></a>
           </div>
         </aside>