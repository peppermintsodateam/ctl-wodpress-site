<section class="bottom-elements bottles-about">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">

                <div class="chemicals-left chemicals-all">
                  <div class="black-filler"></div>
                  <div class="bubbles-wrapper">
                    <div class="bubble big-bubble"></div>
                    <div class="bubble medium-bubble"></div>
                    <div class="bubble medium-bubble xm"></div>
                    <div class="bubble small-bubble"></div>
                    <div class="bubble small-bubble xs"></div>
                  </div>
                </div>

                <div id="yellow-pip" class="yellow-pippet-shape chemicals-all"></div>
                <div class="chemicals-right chemicals-all">

                  <div class="big-bubbles-container">
                    <div class="p-bubbles large-b"></div>
                     <div class="p-bubbles large-b l1"></div>

                     <div class="p-bubbles medium-b"></div>
                     <div class="p-bubbles medium-b m1"></div>
                  </div>

                </div>

                <div class="three-pippets chemicals-all"></div>

              </div>
            </div>
          </div>
        </section>