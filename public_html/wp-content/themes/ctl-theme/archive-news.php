<?php get_header(); ?>

<div class="main-container">

		<div class="container-fluid no-padding news-main-container">
			<div class="row">
				<div class="col-xs-12">
					<div class="archive-main-header">
						<h1 class="archive-header">News</h1>
					</div>
				</div>
			</div>

			 <div class="row news-row news-group">

			 	<?php 
			 	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	                $args = array(
	                    'post_type'=>'news',
	                    'post_status'=>'publish',
	                    'order'=>'DESC',
	                    'orderby'=> 'date',
	                    'posts_per_page'=>4,
	                    'paged' => $paged
	                  );

                $news_loop = new WP_Query($args);

                if($news_loop->have_posts() ) : ?>

                 <?php while($news_loop->have_posts() ) : $news_loop->the_post(); ?>

					<div class="col-xs-12 col-md-6 news-cell-article post-<?php echo ($xyz++%4); ?>">
						
					<article class="news-article <?php echo (++$j % 2 == 0) ? 'evenpost' : 'oddpost'; ?>">
						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
						<header class="article-inner-header" style="background-image: url('<?php echo $thumb['0'];?>')"></header>
						<div class="main-inner">
							<section class="post-content">
								<div class="post-content-inner">

								<section class="data-wrapper">
		                   	 		<div class="data">
		                   	 			<span class="day"><?php echo get_the_date('jS'); ?></span>
		                    			<span class="month"><?php echo get_the_date('M'); ?></span>
		                    		</div>
		                   		</section>

									<header class="post-main-header clearfix">
										 <h2 class="news-header"><?php the_title(); ?></h2>
									</header>
									<p><?php the_excerpt_max_charlength(180); ?></p>
		                      		<a href="<?php the_permalink(); ?>" class="call-btn tran-btn">Read more</a>

								</div>
							</section>
						</div>
					</article>
					
            		</div>

                 <?php endwhile; ?>

                 <div class="col-xs-12">
                 	<div class="pagination_container clearfix">
                 		<?php wp_pagenavi( array( 'query' => $news_loop) );  ?>
                 	</div>
                 </div>

			 </div>
			<?php endif; ?>
		</div>
	</div>

<?php get_footer(); ?>