<?php 

/*--------------------------------
        THEME CONFIGURATION
--------------------------------*/

/*SHORT DEFINE PATH*/
define('THEME_PATH', get_template_directory_uri());

/*LONG DEFINE PATH*/

if(!defined('CTL_THEME_DIR')) {
  
  define('CTL_THEME_DIR', get_theme_root().'/'.get_template().'/');
} 

if(!defined('CTL_THEME_DIR')) {
  define('CTL_THEME_DIR', WP_CONTENT_URL.'/themes/'.get_template().'/');
}

require_once CTL_THEME_DIR.'libs/posttypes.php';

//VERY IMPORTANT TO REMEMBER

/*--------------------------------
        LOAD JQUERY
--------------------------------*/

function load_jquery() {
  if (!is_admin()) {
    wp_deregister_script('jquery');
    wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', false, '1.11.2', false);
    wp_enqueue_script('jquery');
  }
}

add_action("after_setup_theme", "load_jquery");


/*add_action( 'wp_enqueue_scripts', 'preloade_me' );

function preloade_me(){
    wp_register_script( 'preloade_me', get_template_directory_uri() . '/js/vendor/jpreloader.min.js', array('jquery'),"",  false ); 
    wp_enqueue_script('preloade_me');
}


add_action( 'wp_enqueue_scripts', 'preloader' );

function preloader(){
    wp_register_script( 'preloader', get_template_directory_uri() . '/js/vendor/preloader.js', array('jquery'),"",  false ); 
    wp_enqueue_script('preloader');
}*/


add_action( 'wp_enqueue_scripts', 'rotator_js' );

function rotator_js(){
    wp_register_script( 'rotator_js', get_template_directory_uri() . '/js/vendor/owl.carousel.min.js', array('jquery'),"",  false ); 
    wp_enqueue_script('rotator_js');
}



/*--------------------------------
 FEATURED IMAGES IN DASHBOARD
--------------------------------*/

function check_thumbnail_support() {  
 if (!current_theme_supports('post-thumbnails')) {  
   add_theme_support( 'post-thumbnails' );  
   //add_action('init','remove_posttype_thumbnail_support');  
 }  
}  
add_action('after_setup_theme','check_thumbnail_support');



//REMOWE EMPTY TAG WHEN DISPLAYING CONTENT
function remove_empty_tags($content) {
  $tags = array(
    '<p>[' => '[',
    ']</p>' =>']',
    ']<br>' => ']',
    ']<br />' => ']'
  );
  $content = strtr($content, $tags);
  return $content;
  }

add_filter('the_content', 'remove_empty_tags'); 

//REMOWE EMPTY TAG WHEN DISPLAYING CONTENT
//remove_filter ('the_content', 'wpautop');




/*--------------------------------
 FUNCTION REGISTER MENU
--------------------------------*/
function register_ctl_menus() {
  register_nav_menus(
      array(
        'main-menu' => __( 'Main Menu' ),
        'secondary-menu' => __( 'Single Menu' )
      )
    );
  }
  
add_action('init','register_ctl_menus');


/*--------------------------------
 FUNCTION TO CUT TEXT (EXCERPT)
--------------------------------*/
function the_excerpt_max_charlength($charlength) {
  echo cutText(get_the_excerpt(), $charlength);
  }

//funkcja odpowiadajaca za przycinanie tytulu komentarza na stronie glownej do 27 znakow
    function cutText($text, $maxLength){
        
        $maxLength++;

        $return = '';
        if (mb_strlen($text) > $maxLength) {
            $subex = mb_substr($text, 0, $maxLength - 5);
            $exwords = explode(' ', $subex);
            $excut = - ( mb_strlen($exwords[count($exwords) - 1]) );
            if ($excut < 0) {
                $return = mb_substr($subex, 0, $excut);
            } else {
                $return = $subex;
            }
            $return .= '[...]';
        } else {
            $return = $text;
        }
        
        return $return;
    }


/*--------------------------------
 SIDEBARS
--------------------------------*/

if(function_exists(register_sidebar)) {
  $sidebar_list = array(
    array(
      'name'=>'Main sidebar',
      'id'=>'main-widget-list',
      'description'=>'main-widget-area'
      ),

    array(
      'name'=>'Default',
      'id'=>'default-widget',
      'description'=>'This is default widget area'
      ),

    array(

        'name'=>'Footer sidebar',
        'id'=>'footer-sidebar',
        'description'=>'footer-sidebar-area'
      )
    );

  $sidebar_opts = array(
    'before_widget' => '<div id="%1$s" class="widget-box %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3 class="sidebar-main-header">',
    'after_title' => '</h3>'
    );

  foreach($sidebar_list as $sidebar) {
    register_sidebar(array_merge($sidebar, $sidebar_opts));
  }
}//end of sidebar function


 

//Generate menu
    function generate_menu() {
        $pages = get_pages(array(

              'sort_order'=> 'ASC',
              'exclude' => '21, 98, 124',
              'sort_column'=>'menu_order'
          ));

        if(empty($pages)) return;

        $output = '';

        foreach ($pages as $page) {
            $output .= '<li><a href="#' . $page->post_name . '">' . $page->post_title . '</a></li>' . PHP_EOL;
        }

        echo $output;
    }

    /*TEMPLATE PARTS FOR ONE PAGE WORDPRESS LAYOUT*/ 
    function get_part($name) {
      get_template_part('template_parts/' . $name, 'template');
    }

function get_ajax_page() {

    header( "Content-Type: application/json" );
    $id_post = $_POST['post_id'];
    $post_att = get_post($id_post);

    $url_post = get_permalink($id_post);
    $post_att->the_permalink = $url_post;

    echo json_encode($post_att, JSON_UNESCAPED_UNICODE);


}

add_action('wp_ajax_nopriv_get-content-page', 'get_ajax_page');
add_action('wp_ajax_get-content-page', 'get_ajax_page');



if ( ! function_exists( 'custom_post_nav' ) ) :
/**
 * Display navigation to next/previous post when applicable.
 */
function custom_post_nav() {
    // Don't print empty markup if there's nowhere to navigate.
    $previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
    $next     = get_adjacent_post( false, '', false );
 
    if ( ! $next && ! $previous ) {
        return;
    }
    ?>
    <nav class="navigation post-navigation" role="navigation">
        <div class="nav-links">
            <?php
                previous_post_link( '<div class="nav-previous">%link</div>', _x( 'Previous', 'acrylic' ) );
                next_post_link(     '<div class="nav-next">%link</div>',     _x( 'Next',     'acrylic' ) );
            ?>
        </div>
    </nav>
    <?php
}
 
endif;


function related_posts( $post_id, $related_count, $args = array() ) {
  
  $args = wp_parse_args( (array) $args, array(
    'orderby' => 'rand',
    'return'  => 'query', // Valid values are: 'query' (WP_Query object), 'array' (the arguments array)
  ) );
 
  $related_args = array(
    'post_type'      => get_post_type( $post_id ),
    'posts_per_page' => $related_count,
    'post_status'    => 'publish',
    'post__not_in'   => array( $post_id ),
    'orderby'        => $args['orderby'],
    'tax_query'      => array()
  );
 
  $post       = get_post( $post_id );
  $taxonomies = get_object_taxonomies( $post, 'names' );
 
  foreach( $taxonomies as $taxonomy ) {
    $terms = get_the_terms( $post_id, $taxonomy );
    if ( empty( $terms ) ) continue;
    $term_list = wp_list_pluck( $terms, 'slug' );
    $related_args['tax_query'][] = array(
      'taxonomy' => $taxonomy,
      'field'    => 'slug',
      'terms'    => $term_list
    );
  }
 
  if( count( $related_args['tax_query'] ) > 1 ) {
    $related_args['tax_query']['relation'] = 'OR';
  }
 
  if( $args['return'] == 'query' ) {
    return new WP_Query( $related_args );
  } else {
    return $related_args;
  }
}


?>