(function($){

	var doc = $(document);




	var mainHeaderHeight = $('#main-header').outerHeight();
	var container = $(".main-container");
	var FOOTER_HEIGHT = 190;
	var footerHeight = $('#footer-wrapper').outerHeight();
	var leftNavigation = $('#sidebar');
	var switcher = $(".switcher-wrapper #mob-nav");
	var closeMenu = $('.close-menu');




		equalheight = function(container){

		var currentTallest = 0,
		     currentRowStart = 0,
		     rowDivs = new Array(),
		     $el,
		     topPosition = 0;
		 $(container).each(function() {

		   $el = $(this);
		   $($el).height('auto')
		   topPostion = $el.position().top;

		   if (currentRowStart != topPostion) {
		     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		       rowDivs[currentDiv].height(currentTallest);
		     }
		     rowDivs.length = 0; // empty the array
		     currentRowStart = topPostion;
		     currentTallest = $el.height();
		     rowDivs.push($el);
		   } else {
		     rowDivs.push($el);
		     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
		  }
		   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		     rowDivs[currentDiv].height(currentTallest);
		   }
		 });

		}

		if($('body').hasClass('single')) {

			if($('.news-group').length <= 0) {
				$('body').addClass('body-color');
			}else {
				$('body').removeClass('body-color');
			}
		}

	/*SLIDING MENU FROM LEFT TO RIGHT*/
		var options = {
	  		dragLockToAxis: true,
	  		dragBlockHorizontal: true
		};

	delete Hammer.defaults.cssProps.userSelect;
    var hammertime = new Hammer(document.querySelector('body'));


    hammertime.on('swiperight', function(ev) {

    	leftNavigation.removeClass('onCanvas');
    	$('.sliding-panel').removeClass('onCanvas');

    });

    $('#main-header').addClass('shadow-header');


	// Add class to nav switcher on click
	switcher.on("click", function(){
		var $this = $(this);
		$this.toggleClass('open-menu');
		leftNavigation.toggleClass('onCanvas');
	});

	closeMenu.on("click", function(e){
		e.preventDefault();
		var $this = $(this);
		leftNavigation.removeClass('onCanvas');
	});


	$('.second-nav li a, .contact-btn').on('click', function(e){
		e.preventDefault();
		$('.contact-us-panel').toggleClass('onBottom');
		leftNavigation.removeClass('onCanvas');

	});

	$('.single-nav li.contact-link a').on('click', function(e){
		e.preventDefault();
		$('.contact-us-panel').toggleClass('onBottom');
		leftNavigation.removeClass('onCanvas');
	})
	

	$('.close-contact').off('click').on("click", function(e){
		e.preventDefault();
		var $this = $(this);
		$('.contact-us-panel').removeClass('onBottom');
	});


	container.css('margin-bottom', $('#footer-wrapper').outerHeight());
	container.css('padding-bottom', $('#footer-wrapper').outerHeight() -20 );


	doc.ajaxStart(function(event, jqXHR, options) {
		var preloader = $("#preloader");

		if(!preloader.length) {
            preloader = $("<div></div>", {
                "id": "preloader",
                "class": "preloader"
            }).appendTo("body");
        }

        $("#preloader").fadeIn(500);

	});

	 doc.ajaxComplete(function() {

        $("#preloader").fadeOut(500);
        equalheight('.news-article');

    });


	doc.ready(function() {

		var news = $(".news-main-container");
	 	var pageNavi = $('.wp-pagenavi');
	 	var links = $('.wp-pagenavi a');


	 	var byRow = $('body').hasClass('page-template-page-overview');
			$('.ajax-row').each(function() {
         	$(this).children('.cell').matchHeight({
           byRow: byRow
     	});
  	});

	 	

	 	news.on('click', '.wp-pagenavi a', function(e){
	 		e.preventDefault();
	 		var page = $(this).attr("href");

	 		news.load(page + " .news-group, .pagination_container" ,function() {
	 			$("html, body").animate({
                    scrollTop: news.offset().top - mainHeaderHeight
                }, 1500);
	 		});

	 	});


	 	$('.related-news-rotator').owlCarousel({
	        items:1,
	         margin:0,
	         loop:true,
	         responsiveClass:true,
	         nav:false,
	         dots:true,
	         autoplay:true,
	         autoplayTimeout:3000,
	         mouseDrag:true,
	         freeDrag:false,

	    responsive:{
	        0:{
	            items:1
	        },

	        480:{
	            items:1
	        },


	        600:{
	            items:1
	        },

	        1200:{
	            items:2
	        },

	        1400:{
	          items:2
	        },

	        1600:{
	          items:2
	        }
	    }

	  });

	 	$('.related-news-rotator .owl-controls .owl-dot').each(function(i){
	 		$(this).find('span').text(++i);
	 	});
/*var i = 1;


// Added numbers as class to divs
$('.news-cell > div').each(function(index, value){
	var num = index + 1;
	 $(value).attr("class", + num);
});*/




});// End document ready



	$(window).on('load', function(){
		 equalheight('.news-article');

	});


	 if (!isMobile) {
		$(window).on('resize', function(){

			equalheight('.news-article');
			
			setTimeout(function() {
				container.css('margin-bottom', $('#footer-wrapper').outerHeight());
				container.css('padding-bottom', $('#footer-wrapper').outerHeight() -20);
			},100);
		
		});
	}

	 
	
})(jQuery);