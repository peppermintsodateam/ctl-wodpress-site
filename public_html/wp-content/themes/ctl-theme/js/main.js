(function($){

	var browserWindow = $(window);
	var section = $('.section');
	var yellowPip = $('#yellow-pip');
	var ON_POP_STATE_SAFARI_BUG_TIME = 100;
	var mainHeaderHeight = $('#main-header').outerHeight();
	var container = $(".main-container");
	var SECTION_ANIMATE_TIME = 500;
	var closeTopic = $('.close-topic');
	var FOOTER_HEIGHT = 190;
	var leftNavigation = $('#sidebar');
	var switcher = $(".switcher-wrapper #mob-nav");
	var closeMenu = $('.close-menu');
	var topHero = $('.top-hero img');

	var scrollingContent = document.getElementById("scroll-section");
     new ScrollFix(scrollingContent);



	/*SLIDING MENU FROM LEFT TO RIGHT*/
		var options = {
	  		dragLockToAxis: true,
	  		dragBlockHorizontal: true
		};

		
	// Enable hammer text selection
	delete Hammer.defaults.cssProps.userSelect;
    var hammertime = new Hammer(document.querySelector('body'));


    hammertime.on('swiperight', function(ev) {

    	leftNavigation.removeClass('onCanvas');
    	$('.sliding-panel').removeClass('onCanvas');

    });


	// Add class to nav switcher on click
	switcher.on("click", function(){
		var $this = $(this);
		$this.toggleClass('open-menu');
		leftNavigation.toggleClass('onCanvas');
	});

	closeMenu.on("click", function(e){
		e.preventDefault();
		var $this = $(this);
		leftNavigation.removeClass('onCanvas');
	});


	$('.second-nav li a, .contact-btn').on('click', function(e){
		e.preventDefault();
		$('.contact-us-panel').toggleClass('onBottom');
		leftNavigation.removeClass('onCanvas');

	});

	$('.close-contact').off('click').on("click", function(e){
		e.preventDefault();
		var $this = $(this);
		$('.contact-us-panel').removeClass('onBottom');
	});

	// Scroll between sections plugin
	$.fn.scrollToSection = function() {
		this.first().parent().addClass('current');

		return this.on('click', function(e){
			e.preventDefault();

			var $this = $(this);
			var targetId = $this.attr('href');
			if(!targetId) return;
			var target = $(targetId);
			var offset = target.offset().top - mainHeaderHeight;

			if(target.attr("id")==="what-we-do") offset = 0;


			$('html, body')
				.stop()
				.animate({

					scrollTop:offset

				}, {
					duration:800,
					easing: "easeInOutExpo"
				});

				/*if (typeof history.pushState !== "undefined") {
				 	var id = target.attr('id');
				 	history.pushState({id: id}, '', "#" + id);
				 }*/

				$(this).parent().siblings(".current").removeClass("current");
            	$(this).parent().addClass("current");

		});

	};


	$('.read-more-btn').on('click', function(e){
		e.preventDefault();
		var that = $(this);
		var targetId = that.attr('href');
		if(!targetId) return;
		var target = $(targetId);
		var offset = target.offset().top - mainHeaderHeight;

		$('html,body')
                .stop()
                .animate({
                  scrollTop:offset
                }, {
                  	duration: 600,
                 	easing: "swing"
                });
	});


	$('.clients-btn').on('click', function(e){
		e.preventDefault();
		var that = $(this);
		var targetId = that.attr('href');
		if(!targetId) return;
		var target = $(targetId);
		var offset = target.offset().top - mainHeaderHeight;

		$('html,body')
                .stop()
                .animate({
                  scrollTop:offset
                }, {
                  	duration: 600,
                 	easing: "swing"
                });
	});



	//container.css('margin-bottom', FOOTER_HEIGHT);
	container.css('margin-bottom', $('#footer-wrapper').outerHeight());
	container.css('padding-bottom', $('#footer-wrapper').outerHeight());


	$('.home-panel').on('click', function(e){
		e.preventDefault();

		var that = $(this),
			targetId = that.attr("href");
			if(!targetId) return;

			var target = $(targetId);


           target.toggleClass("onCanvas");
           $('.about-slide').removeClass("onCanvas");
           $('.services-slide').removeClass("onCanvas");
           $('.realising-slide').removeClass("onCanvas");

           	var offset = target.offset().top - mainHeaderHeight;
			var page = $('html, body');

			 page
                .stop(true)
                .animate({
                  scrollTop:offset
                }, {
                  duration: 600,
                  easing: "swing"

                });

                return false;
	});


	$('.about-panel').on('click', function(e){
		e.preventDefault();

		var that = $(this),
			targetId = that.attr("href");
			if(!targetId) return;

			var target = $(targetId);



           target.toggleClass("onCanvas");
           $('.home-slide').removeClass("onCanvas");
           $('.services-slide').removeClass("onCanvas");
           $('.realising-slide').removeClass("onCanvas");

           	var offset = target.offset().top - mainHeaderHeight;
			var page = $('html, body');

			 page
                .stop(true)
                .animate({
                  scrollTop:offset
                }, {
                  duration: 600,
                  easing: "swing"

                });

                return false;

	});

	$('.services-panel').on('click', function(e){
		e.preventDefault();

		var that = $(this),
			targetId = that.attr("href");
			if(!targetId) return;

			var target = $(targetId);


           target.toggleClass("onCanvas");
           $('.home-slide').removeClass("onCanvas");
           $('.about-slide').removeClass("onCanvas");
           $('.realising-slide').removeClass("onCanvas");

           	var offset = target.offset().top - mainHeaderHeight;
			var page = $('html, body');

			 page
                .stop(true)
                .animate({
                  scrollTop:offset
                }, {
                  duration: 600,
                  easing: "swing"

                });

                return false;

	});

	$('.realising-panel').on('click', function(e){
		e.preventDefault();

		var that = $(this),
			targetId = that.attr("href");
			if(!targetId) return;

			var target = $(targetId);




           target.toggleClass("onCanvas");
           $('.home-slide').removeClass("onCanvas");
           $('.about-slide').removeClass("onCanvas");
           $('.services-slide').removeClass("onCanvas");

           	var offset = target.offset().top - mainHeaderHeight;
			var page = $('html, body');

			 page
                .stop(true)
                .animate({
                  scrollTop:offset
                }, {
                  duration: 600,
                  easing: "swing"

                });

                return false;
	});


	$('.who-panel').on('click', function(e){
		e.preventDefault();

		var that = $(this),
			targetId = that.attr("href");
			if(!targetId) return;

			var target = $(targetId);


           target.toggleClass("onCanvas");
           $('.home-slide').removeClass("onCanvas");
           $('.about-slide').removeClass("onCanvas");
           $('.services-slide').removeClass("onCanvas");
           $('.realising-slide').removeClass("onCanvas");

           var offset = target.offset().top - mainHeaderHeight;
			var page = $('html, body');

			 page
                .stop(true)
                .animate({
                  scrollTop:offset
                }, {
                  duration: 600,
                  easing: "swing"

                });

                return false;
	});


	closeTopic.on('click', function(e){
		e.preventDefault();

		// Close buttom on click removes hash tag from slide
		/*history.pushState('', document.title, window.location.pathname);*/

		$('.home-slide').removeClass("onCanvas");
		$('.about-slide').removeClass("onCanvas");
		$('.services-slide').removeClass("onCanvas");
		$('.realising-slide').removeClass("onCanvas");
	});


/*MATCHING HEIGHTS PLUGIN*/
var byRow = $('body').hasClass('home-body');

 $('.row-wrapper').each(function() {
         $(this).children('.cell').matchHeight({
           byRow: byRow
     });
  });


 $('.row-panels').each(function() {
         $(this).children('.panel-height').matchHeight({
           byRow: byRow
     });
  });




 function verticalCenter() {
 	$('.section-top').flexVerticalCenter({ cssAttribute: 'margin-top', verticalOffset: '-80px' });
 	$('#bottom-details > .container').flexVerticalCenter({ cssAttribute: 'margin-top', verticalOffset: '50px' });
 }

 /* function testimonialHeight() {
 	$('.half-width').css('height', $(window).height() - $('#main-header').outerHeight() - $('#footer-wrapper').outerHeight());
 }*/

 //testimonialHeight();

		$(function(){


			if ($(window).width() > 1024) {
    			if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
					skrollr.init({
					forceHeight: false
					});
				}
  			} else {
  				skrollr.init().destroy();
  			}

		   
		    
			verticalCenter();

			// Testimonial slider goes here
			$('.testimonial-rotator').owlCarousel({
		        items:1,
		        loop:true,
		        responsiveClass:true,
		        nav:false,
		        dots:true,
		        autoplay:true,
		        autoplayTimeout:6000,
		        mouseDrag:false,
		        freeDrag:false,
		        onInitialize:startFunction,
		        onChanged:onChanged,

		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        },

		        1400:{
		          items:1
		        }
		    }

		  });

			function startFunction() {
				console.log('start');
			}

			function onChanged() {
				console.log('next slide');
			}

			


		$('.clients-rotator').owlCarousel({
	        items:1,
	        loop:true,
	        responsiveClass:true,
	        nav:false,
	        dots:true,
	        autoplay:true,
	        autoplayTimeout:3000,
	        mouseDrag:false,
	        freeDrag:false,

	    responsive:{
	        0:{
	            items:1
	        },

	        480:{
	            items:2
	        },


	        600:{
	            items:3
	        },

	         1000:{
	            items:3
	        },

	        1200:{
	            items:4
	        },

	        1400:{
	          items:4
	        },

	        1600:{
	          items:4
	        }
	    }

	  });


		$('.team-rotator').owlCarousel({
	        items:1,
	        loop:true,
	        responsiveClass:true,
	        nav:true,
	        dots:true,
	        autoplay:false,
	        autoplayTimeout:5000,
	        mouseDrag:false,
	        freeDrag:false,

	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1200:{
	            items:3
	        },

	        1400:{
	          items:3
	        },

	        1600:{
	          items:4
	        }
	    }

	  });

	}); // End of document ready

	// Open page in new window with new hash.
	function findHash() {
		$('#sidebar nav ul li a').filter(function(){
			return $(this).attr("href") === window.location.hash;
		}).trigger("click");
	}

	findHash();

	$('#slogan-banner').removeClass('section');


    var offsetTop = 0;

	$(window).on('scroll', function(){

		var TOP_SCROLL = 100;
		var scroll = $(window).scrollTop();
		var winHeight = $(window).height();
		var OFFSET_BOTTOM = winHeight - 1/1.2 * winHeight;

		// Bottles rotates in the half of the window and leeking starts

		var bottlesTop = $('.bottle-rotate').offset().top - scroll + OFFSET_BOTTOM + $('.bottle-rotate').height();
		var lineWrapper = $('.line-wrapper');

		// Adding class to top header.
		if(scroll > TOP_SCROLL) {
			$('#main-header').addClass('shadow-header ')
		}else {
			$('#main-header').removeClass('shadow-header');
		}

		if (winHeight > bottlesTop + 50) {
			// Animation here
			
            if (offsetTop === 0) {

            	// Line progress
                offsetTop = OFFSET_BOTTOM ; //$(window).scrollTop();
                
            }
		} else {
			
            offsetTop = 0;
		}

		if($(window).scrollTop() > 147) {
			$('.bottle-rotate').addClass('rotate-me');
			$('.bottles-right-3').addClass('shake1');
			$('.bottles-right-4').addClass('shake2');
		}else {
			$('.bottle-rotate').removeClass('rotate-me');
			$('.bottles-right-3').removeClass('shake1');
			$('.bottles-right-4').removeClass('shake2');
		}

		// Adding active class to section on scroll
        $('.section').each(function(i) {
        	offset = $(this).offset().top - mainHeaderHeight;
        	if(Math.floor(offset) - $(window).scrollTop() <= 0) {
				var current = $(".section").filter(".current-section");
				current.removeClass("current-section");
				section = $(this);
				section.addClass("current-section");
			}
        });


        // Highlight main navigation links on scroll.

		$('.section').each(function(index){
			var that = $(this),
			offset = $(this).position().top - mainHeaderHeight;
			

			if(Math.floor(offset) - $(window).scrollTop() <= 0) {
				$('#sidebar .menu-inner nav .main-nav li').siblings().removeClass('current');
				$("#sidebar .menu-inner nav .main-nav li").eq(index).addClass('current');
			}
		});

	


		/*ADDING #id TO EACH SECTION ON SCROLL*/
		setTimeout(function () {
           
  			if (typeof history.pushState !== "undefined") {
				var id = $(".section").filter(".current-section").attr("id");
			history.pushState({id: id}, '', "#" + id);
		}

        }, ON_POP_STATE_SAFARI_BUG_TIME);

       
        var ratioRedLine = 1.3;// Set line white faster
    	var ratioWhiteLine = 0.8;

    	var homeSectionHeight = $('#what-we-do').height();
        var aboutSectionHeight = $('#who-we-are').height();
        var servicesSectionHeight = $('#how-we-do-it').height();

       lineWrapper.height($('.pippet-black-wrapper').position().top);

        var winScroll = $(window).scrollTop();

        // Offset top pipe's element according to its section - important to add Class
        var pipeScrollTop = aboutSectionHeight - $('#yellow-pip').height();

        var offset = offsetTop || homeSectionHeight;

        // Extra value added to slow down the line
        var percentageRedComplete = (winScroll - offset) / (aboutSectionHeight - mainHeaderHeight + 350);


        var redLineHeight = percentageRedComplete * ratioRedLine * aboutSectionHeight;
        var whiteLineHeight = (percentageRedComplete * ratioRedLine - 1) * ratioWhiteLine * servicesSectionHeight;

        $('.line-white').css({ height: redLineHeight });

        // White line dotyka lejka.
        if (redLineHeight >= pipeScrollTop) {
            $('.bottles-about').addClass('start-filling');
        } else {
            $('.bottles-about').removeClass('start-filling');
        }

        if($('.bottles-about').hasClass('start-filling')) {
        	$('.black-filler').addClass('fill-black');


        } else {
        	$('.black-filler').removeClass('fill-black');
        }

        // Red line dotyka końca sekcji.
        if (percentageRedComplete * ratioRedLine >= 1) {

            $('.line-black').css({ height: whiteLineHeight });
        } else {
            $('.line-black').css({ height: 0 });
        }

        // Moment when black line touches div

        if (whiteLineHeight >= $('.pippet-black-wrapper').position().top - 60) {
        	

           $('.filler-inside').stop(true).animate({
	        		height: 131
	    		}, 500);

            
        } else {

            $('.filler-inside').stop(true).animate({
	        		height: 0
	    		}, 500);
        	}

	        if($('.jar-white-wrapper').hasClass('skrollable-after')) {
				$('.bubbles-container').addClass('start-bubbles');

		} else {
			$('.bubbles-container').removeClass('start-bubbles');
		}

		if($('#realising-a-return').hasClass('current-section')) {
			$('.flame').fadeIn('slow');
		} else {
			$('.flame').fadeOut('fast');
		}

		if($('.pippet-long-wrapper').hasClass('skrollable-after')) {
			$('.pippet-grey-filler').addClass('start-drawing');
		}



		if($(window).scrollTop() >= 3430) {
			$('#triangle-svg').attr('class', 'tr-shape animate-shape');
		} 

		if($(window).scrollTop() < 2400) {

			$('.triangle-wrapper').removeClass('animate-shape');
			$('.pippet-grey-filler').removeClass('start-drawing');
			$('#triangle-svg').attr('class', 'tr-shape');
		}
	});

	// Calling plugins
	section.first().addClass("active-section");
	$('.nav-index .main-nav').find('a').scrollToSection();


	$(window).on('load', function(){
		verticalCenter();
		findHash();
		topHero.addClass('zoomIn');
		var hash = window.location.hash;

		if(hash ==="undefined") {

			window.location.hash = "#what-we-do";

		}

		// Function helps to direct to proper offset top for each section

		setTimeout(function () {
           
  			/*if(window.location.hash !=='') {
			$("html, body").stop(true).scrollTop($(".section").filter(function() {
			return $(this).attr("id") === window.location.hash.replace("#",'');
	  		}).offset().top); 
		}*/

		findHash();

        }, 120);



		
		$(window).trigger('scroll');// Calling drawing on load

		// Pokazywanie od razu artykułu po załadowaniu.
        
        /*setTimeout(function () {
            window.onpopstate = function(event) {
  				history.go(-5);
  				$('.home-slide').removeClass("onCanvas");
  				$('.about-slide').removeClass("onCanvas");
  				$('.services-slide').removeClass("onCanvas");
  				$('.realising-slide').removeClass("onCanvas");
			};



        }, ON_POP_STATE_SAFARI_BUG_TIME);*/


        $('.nav-index .main-nav').find('a').scrollToSection();

	});


	if (!isMobile) {
		$(window).on('resize', function(){

			//testimonialHeight();

			 if ($(window).width() > 1024) {
      			var s = skrollr.init({forceHeight:false});
    		}
			
			 if ($(window).width() < 1024) {
      			skrollr.init().destroy(); 
    		}

			setTimeout(function() {
				verticalCenter();
				container.css('padding-bottom', $('#footer-wrapper').outerHeight());
				container.css('margin-bottom', $('#footer-wrapper').outerHeight());
			},100);
		
		});
	}

//$(window).trigger('resize');
	

})(jQuery);
