(function($){

    $(function(){

        //calling jPreLoader
        $('body').jpreLoader({
            splashID: "#jSplash",
            loaderVPos: '50%',
            autoClose: true,
            splashFunction: function() {  
                //passing Splash Screen script to jPreLoader
                $('#jSplash').children('section').not('.selected').hide();
                $('#jSplash').hide().fadeIn(800);
            }

        }, function() { //callback function
            //clearInterval(timer);
        });


    });

        
})(jQuery);