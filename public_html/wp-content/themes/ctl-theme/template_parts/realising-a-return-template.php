<div class="jar-white-wrapper"
             data-150-top="top: -220px;"
             data-top-bottom="top:525px;"
        >
          <div class="jar-white">
           
          </div>
        </div>

        <div class="bubbles-container">
          
        </div>

        <div class="flame"></div>
        
        <div class="section-top">
           <div class="container">
             <div class="row row-wrapper">

             <div class="col-sm-12 pull-right col-md-5 cell">
                  <div class="cell cell-left">
                    <div class="cell-inside">
                      <figure class="hero-wrapper">
                        <img src="<?php echo THEME_PATH; ?>/img/realising-return.png" class="img-responsive wow zoomIn" alt="realising-return">
                      </figure>
                      
                    </div>
                  </div>
              </div>

               <div class="col-sm-12 col-md-7 cell">
                  <div class="cell-left">

                    <div class="cell-inside">

                      <header class="top-headers left-align-header">
                        <h2 class="home-header white-header">
                          <?php if(have_rows('main_header')): ?>
                              <?php while(have_rows('main_header')):the_row();  ?>
                                <span><?php the_sub_field('inner_header') ?></span>
                            <?php endwhile; ?><?php endif; ?>
                        </h2>
                      </header>

                        <div class="top-text-box text-box-left">
                          <?php the_field('main_slogan_text') ?>

                          <div class="button-wrapper left-button-w">
                            <a class="call-btn call-b-btn realising-panel" href="#leadership-development"><?php the_field('call_to_action_text') ?></a>
                          </div>

                      </div>
                    </div>

                  </div>
                </div>

             </div>
           </div>
        </div>

            <div id="leadership-development" class="sliding-panel realising-slide offCanvas">
                <section class="inner-section">
                  <div class="topic-content-inside padding-realising">
                    <div class="container">

                       <div class="row row-panels">


                         <div class="col-xs-12 col-md-6 pull-right panel-inside-right panel-height">
                           <figure class="hero-wrapper hero-small">
                              <img src="<?php echo THEME_PATH; ?>/img/realising-return.png" class="img-responsive" alt="realising-return">
                            </figure>
                         </div>


                         <div class="col-xs-12 col-md-6 panel-inside-left panel-height">
                           <header class="inside-panel-header">
                              <h3 class="h-in white-in">
                                <?php if(have_rows('header_main_inner')): ?>
                                    <?php while(have_rows('header_main_inner')):the_row();  ?>
                                      <span><?php the_sub_field('header_small_inner') ?></span>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                              </h3>
                            </header>

                              <?php the_field('inner_slide_content') ?>
                              <div class="case-wrapper"><?php the_field('case_study_content'); ?></div>
                              <div class="case-name"><?php the_field('name'); ?></div>
                              <div class="case-company"><?php the_field('company'); ?></div>
                            <a href="#testimonials" class="call-btn read-more-btn"><?php the_field('case_btn'); ?></a>

                         </div>


                       </div>

                    </div>
                  </div>

            </section>
              <a class="close-topic white-close" href="#"></a>
            </div>