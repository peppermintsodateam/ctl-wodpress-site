<div class="container-fluid no-padding">


          <div class="row news-row">

        
            <?php 
                $args = array(
                    'post_type'=>'news',
                    'post_status'=>'publish',
                    'posts_per_page'=>1
                  );

                $news_loop = new WP_Query($args);

                if($news_loop->have_posts() ) :

             ?>

           <?php while($news_loop->have_posts() ) : $news_loop->the_post(); ?>
              <div class="col-xs-12 col-md-6 news-cell">
                <article class="news-box news-l">
                  <div class="news-inside">
                    <div class="news-cont">
                     <h2 class="news-header">News</h2>
                      <h3 class="news-smaller-header"><?php the_title(); ?></h3>
                      <p><?php the_excerpt_max_charlength(140); ?></p>
                      <a href="<?php the_permalink(); ?>" class="call-btn tran-btn">Read more</a>
                    </div>
                  </div>
                </article>

            </div>
           <?php endwhile; ?>
          
        <?php endif; ?>

              <?php 
                  $args = array(
                      'post_type'=>'events',
                      'post_status'=>'publish',
                      'posts_per_page'=>1
                    );

                  $events_loop = new WP_Query($args);

                  if($events_loop->have_posts() ) :

               ?>

             <?php while($events_loop->have_posts() ) : $events_loop->the_post(); ?>

              <div class="col-xs-12 col-md-6 news-cell">
               <article class="news-box news-r">
                 <div class="news-inside">
                  <div class="news-cont">
                    <h2 class="news-header">Events</h2>
                    <h3 class="news-smaller-header"><?php the_title(); ?></h3>
                    <p><?php the_excerpt_max_charlength(140); ?></p>
                    <a href="<?php the_permalink(); ?>" class="call-btn tran-btn">Read more</a>
                  </div>
                </div>
               </article>
            </div>

            <?php endwhile; ?>

          <?php endif; ?>

            


          </div>
        </div>

          