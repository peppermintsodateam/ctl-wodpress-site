<div class="container">
          <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
              <div class="contact-details">
                <img src="<?php echo THEME_PATH;?>/img/logo-contact.png" alt="CTL bottom logo" class="img-responsive">

                <div class="contact-wrapper">
                  <?php the_field('bottom_details'); ?>
                </div>
              </div>
            </div>
          </div>
        </div>