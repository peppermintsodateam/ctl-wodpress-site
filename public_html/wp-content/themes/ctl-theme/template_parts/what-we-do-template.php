<div class="section-top">
          <div class="container">
              <div class="row row-wrapper">


                <div class="col-sm-12  pull-right col-md-5 cell">
                  <div class="cell cell-right">
                    <div class="cell-inside">
                      <figure class="hero-wrapper top-hero">
                        <img src="<?php echo THEME_PATH; ?>/img/what-we-do.png" class="img-responsive wow" alt="what-we-do" data-wow-delay="1.0s">
                      </figure>
                    </div>
                  </div>
                </div>

                <div class="col-sm-12 col-md-7 cell">
                  <div class="cell-left">

                    <div class="cell-inside">

                      <header class="top-headers left-align-header">
                        <h1 class="home-header black-header">
                          <?php if(have_rows('main_header')): ?>
                            <?php while(have_rows('main_header')):the_row();  ?>
                              <span><?php the_sub_field('inner_header') ?></span>
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </h1>
                      </header>

                        <div class="top-text-box text-box-left">
                          <?php the_field('main_slogan_text') ?>

                          <div class="button-wrapper left-button-w">
                            <a class="call-btn call-b-w-btn home-panel" href="#what-we-do-more"><?php the_field('call_to_action_text') ?></a>
                          </div>

                      </div>
                    </div>

                  </div>
                </div>
              </div>
          </div>
        </div>

        <?php get_template_part('templates/home', 'bottles'); ?>


        <div id="what-we-do-more" class="sliding-panel home-slide offCanvas" data-id="1">

            <section class="inner-section">
              <div class="topic-content-inside">
                <div class="container">

                   <div class="row row-panels">

                       <div class="col-xs-12 col-md-6 pull-right panel-inside-right panel-height">
                         <figure class="hero-wrapper hero-small">
                            <img src="<?php echo THEME_PATH; ?>/img/what-we-do.png" class="img-responsive" alt="what-we-do">
                          </figure>
                       </div>

                     <div class="col-xs-12 col-md-6 panel-inside-left panel-height">
                       <header class="inside-panel-header">
                          <h3 class="h-in black-in">
                             <?php if(have_rows('header_main_inner')): ?>
                              <?php while(have_rows('header_main_inner')):the_row();  ?>
                                <span><?php the_sub_field('header_small_inner') ?></span>
                              <?php endwhile; ?>
                             <?php endif; ?>
                          </h3>
                        </header>
                        <?php the_field('inner_slide_content') ?>

                        

                        <ul class="list-inside-home">

                          <?php if(have_rows('ingredients_for_success')): ?>
                             <?php while(have_rows('ingredients_for_success')):the_row();  ?>
                              <li><span><?php the_sub_field('ingredient') ?></span></li>
                             <?php endwhile; ?>
                          <?php ?>

                         <?php endif; ?>
                          
                      </ul>
                     </div>


                   </div>
                </div>
              </div>
            </section>

          <a class="close-topic dark-close" href="#"></a>
        </div>
        