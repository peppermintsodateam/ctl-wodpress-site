<div class="half-width orange">
          <div class="middle left">
            <h2 class="testimonial-header white-header">
                <span>Don't just </span>  <span>take it</span> <span>from us</span>
            </h2>
          </div>
        </div>

        <div class="half-width slider-section">
          <div class="middle rotator-wrapper">
            <div class="rotator-content">

            <?php 

              $args = array(
                'post_type'=>'testimonials',
                'posts_per_page'=> -1
                );

              $testimonial_loop = new WP_Query($args);

              if($testimonial_loop->have_posts() ) :

            ?>

              <div class="testimonial-rotator">

                <?php while($testimonial_loop->have_posts() ) : $testimonial_loop->the_post(); ?>

                  <div class="owl-item">
                     <div class="testimonial-inner">
                        <section class="testimonial-header-inside">
                          
                            <?php the_content(); ?>
                          
                        </section>
                          <ul class="testimonial-list">
                            <li><?php the_field('name'); ?></li>
                             <li><?php the_field('company'); ?></li>
                          </ul>
                      
                     </div>
                  </div>

                <?php endwhile; ?>
              </div>

            <?php endif; ?>


            </div>
          </div>
          <div class="mask"></div>
        </div>