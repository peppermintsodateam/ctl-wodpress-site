<div class="line-white"></div>

        <div class="section-top">
          <div class="container">
            <div class="row row-wrapper">

            <div class="col-sm-12 col-md-5 cell">
                <div class="cell cell-left">
                    <div class="cell-inside cell-top">
                      <figure class="hero-wrapper">
                        <img src="<?php echo THEME_PATH; ?>/img/who-we-are.png" class="img-responsive wow zoomIn" alt="who-we-are">
                      </figure>
                    </div>
                  </div>
              </div>

            <div class="col-sm-12 col-md-7 cell">
             
                <div class="cell-right">
                  <div class="cell-inside">
                    <header class="top-headers right-align-header">
                        <h2 class="home-header black-header">
                           <?php if(have_rows('main_header')): ?>
                              <?php while(have_rows('main_header')):the_row();  ?>
                                <span><?php the_sub_field('inner_header') ?></span>
                            <?php endwhile; ?><?php endif; ?>
                      </h2>
                    </header>

                    <div class="top-text-box text-box-right">
                      <?php the_field('main_slogan_text') ?>
                          <div class="button-wrapper right-button-w">
                            <a class="call-btn call-b-btn about-panel" href="#meet-our-consultants"><?php the_field('call_to_action_text') ?></a>
                        </div>
                    </div>
                  </div>
                </div>
          
                </div>
            </div>
          </div>
        </div>

        <?php get_template_part('templates/bottom', 'chemicals'); ?>

        <div id="meet-our-consultants" class="sliding-panel about-slide offCanvas">
               <?php get_template_part('templates/team', 'template'); ?>
          <a class="close-topic white-close" href="#"></a>
        </div>


