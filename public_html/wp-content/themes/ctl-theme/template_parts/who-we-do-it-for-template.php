 <div class="pippet-long-wrapper"
              data-600-top="top: -400px;"
             data-80-top="top:240px;"
        >

          <div id="pippet-long-full">
            <div class="pippet-grey-filler"></div>
          </div>
          <div class="mask-pippet"></div>
        </div>

        <div class="section-top">
          <div class="container">
            <div class="row row-wrapper">

            <div class="col-sm-12 col-md-5 cell">
                <div class="cell cell-left">
                    <div class="cell-inside">
                      <figure class="hero-wrapper change-size">
                        <img src="<?php echo THEME_PATH; ?>/img/who-do-we-do-it-for.png" class="img-responsive wow zoomIn" alt="who-do-we-do-it">
                      </figure>
                    </div>
                  </div>
              </div>

            <div class="col-sm-12 col-md-7 cell">
             
                <div class="cell-right">
                  <div class="cell-inside">
                    <header class="top-headers right-align-header">
                        <h2 class="home-header black-header">
                        <?php if(have_rows('main_header')): ?>
                              <?php while(have_rows('main_header')):the_row();  ?>
                                <span><?php the_sub_field('inner_header') ?></span>
                            <?php endwhile; ?><?php endif; ?>
                          
                      </h2>
                    </header>

                      <div class="top-text-box text-box-right">
                          <?php the_field('main_slogan_text') ?>
                        
                            <div class="button-wrapper right-button-w">
                              <a class="call-btn call-w-btn clients-btn" href="#clients"><?php the_field('call_to_action_text') ?></a>
                          </div>
                      </div>

                  </div>
                </div>

            </div>

            </div>
          </div>
        </div>

      <?php get_template_part('templates/triangle', 'svg'); ?>
 