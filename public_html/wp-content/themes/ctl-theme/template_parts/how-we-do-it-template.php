<div class="line-wrapper">
          <div class="line-black"></div>
        </div>

        <div class="section-top">
          <div class="container">
            <div class="row row-wrapper">

              <div class="col-sm-12 pull-right col-md-5 cell">
                  <div class="cell cell-left">
                    <div class="cell-inside">
                      <figure class="hero-wrapper change-size">
                        <img src="<?php echo THEME_PATH; ?>/img/how-we-do-it.png" class="img-responsive wow zoomIn" alt="how-we-do-it">
                      </figure>
                      
                    </div>
                  </div>
              </div>


              <div class="col-sm-12 col-md-7 cell">
                  <div class="cell-left">

                    <div class="cell-inside">

                      <header class="top-headers left-align-header">
                        <h2 class="home-header black-header">
                          <?php if(have_rows('main_header')): ?>
                              <?php while(have_rows('main_header')):the_row();  ?>
                                <span><?php the_sub_field('inner_header') ?></span>
                            <?php endwhile; ?><?php endif; ?>
                        </h2>
                      </header>

                        <div class="top-text-box text-box-left">
                          <?php the_field('main_slogan_text') ?>

                          <div class="button-wrapper left-button-w">
                            <a class="call-btn call-y-btn services-panel" href="#our-services"><?php the_field('call_to_action_text') ?></a>
                          </div>

                      </div>
                    </div>

                  </div>
                </div>
            </div>
          </div>
        </div> 

             <?php get_template_part('templates/pippet', 'template'); ?>

             <div id="our-services" class="sliding-panel services-slide offCanvas">
                <section id="scroll-section" class="inner-section">
                  <div class="topic-content-inside padding-topic">
                    <div class="container">
                   <div class="row row-panels">

                   <div class="col-xs-12 col-md-6 panel-inside-right panel-height">
                     <figure class="hero-wrapper hero-small">
                       <img src="<?php echo THEME_PATH; ?>/img/how-we-do-it.png" class="img-responsive" alt="how-we-do-it">
                   </div>


                     <div class="col-xs-12 col-md-6 panel-inside-left panel-height">

                        <header class="inside-panel-header">
                          <h3 class="h-in black-in">
                            <?php if(have_rows('header_main_inner')): ?>
                              <?php while(have_rows('header_main_inner')):the_row();  ?>
                                <span><?php the_sub_field('header_small_inner') ?></span>
                              <?php endwhile; ?>
                             <?php endif; ?>
                          </h3>
                        </header>
                        <?php the_field('inner_slide_content') ?>

                       
                        <ul class="list-inside-services">
                            
                            <?php if(have_rows('ingredients_for_success')): ?>
                             <?php while(have_rows('ingredients_for_success')):the_row();  ?>
                              <li><span><?php the_sub_field('ingredient') ?></span></li>
                             <?php endwhile; ?>
                          <?php ?>

                         <?php endif; ?>

                      </ul>

                        <?php the_field('extra_parapraph'); ?>
                        <a class="call-btn call-t-btn call-page" href="<?php echo get_page_link(98); ?>"><?php the_field('extra_text_button') ?></a>

                       <!--<ul class="button-list">
                         <li><a class="call-btn call-t-btn" href="#">One day course</a></li>
                         <li><a class="call-btn call-t-btn" href="#">Two day course</a></li>
                         <li><a class="call-btn call-t-btn" href="#">Three day course</a></li>
                       </ul> -->


                     </div>


                   </div>
                </div>
                  </div>
                </section>

              <a class="close-topic white-close" href="#"></a>
            </div> 