<div class="container">
          <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
              <h3 class="subheader-section black-sub">
                <?php the_title(); ?>
              </h3>
            </div>
          </div>
          <div class="row">
            <section class="client-logos">

              <?php
                $args = array(

                  'post_type'=>'clients',
                  'posts_per_page'=> -1

                  );

                $clients_loop = new WP_Query($args);
                if($clients_loop->have_posts() ) :
               ?>

              <div class="clients-rotator">
                <?php while($clients_loop->have_posts() ) : $clients_loop->the_post(); ?>

                  <div class="owl-item">
                    <div class="box-logo-main">
                      <?php if ( has_post_thumbnail() ) : ?>

                        <a href="#" class="logo-box">
                          <?php the_post_thumbnail( 'full', array('class' => 'img-responsive' ) ); ?>
                        </a>

                      <?php endif; ?>
                    </div>
                  </div>

                <?php endwhile; ?>
              </div>

            <?php endif; ?>

            </section>
          </div>
        </div>