 <footer id="footer-wrapper">
      <div class="footer-inside clearfix">
        <div class="footer-left"></div>

        <div class="footer-middle">

         
          
              <div class="footer-content clearfix">
                <div class="left-footer pull-left">
                   <header class="footer-header"><h3>Ready to reboot?</h3></header>
                   <div class="button-wrapper-contact"><a href="#" class="call-btn contact-btn">Contact Us</a></div>
                </div>
                <div class="right-footer pull-right">
                  <?php echo do_shortcode('[cn-social-icon]') ?>
                </div>
              </div>
          
         

        </div>

        <div class="footer-right"></div>
      </div>
    </footer>

    <?php get_template_part('templates/contact', 'form'); ?>

    <?php wp_footer(); ?>
  
    <script src="<?php echo THEME_PATH; ?>/js/vendor/enquire.min.js"></script>
    <script src="<?php echo THEME_PATH; ?>/js/vendor/mobile.js"></script>
    <script src="<?php echo THEME_PATH; ?>/js/vendor/jquery.matchHeight.js"></script>
    <script src="<?php echo THEME_PATH; ?>/js/vendor/hammer.js"></script>
    <script src="<?php echo THEME_PATH; ?>/js/vendor/hammer-time.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="<?php echo THEME_PATH; ?>/js/script.js"></script>


  </body>
</html>