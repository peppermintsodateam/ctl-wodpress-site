<?php get_header('single'); ?>
<?php the_post(); ?>

<div class="main-container">
	<section class="post-wrapper">
		<div class="container-fluid no-padding">
			<div class="row">
				<div class="col-xs-12 post-<?php echo ($xyz++%4); ?>">
					
					<article class="news-article single-article">
						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
						<header class="article-inner-header" style="background-image: url('<?php echo $thumb['0'];?>')"></header>
						<div class="main-inner">
							<section class="post-content">
								<div class="post-content-inner">

								<section class="data-wrapper">
		                   	 		<div class="data">
		                   	 			<span class="day"><?php echo get_the_date('jS'); ?></span>
		                    			<span class="month"><?php echo get_the_date('M'); ?></span>
		                    		</div>
		                   		</section>

									<header class="post-main-header clearfix">
										 <h1 class="news-header pull-left"><?php the_title(); ?></h1>

										 <ul class="social-share-list">
										 	<li>
										 		<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share on Facebook.">
										 			<i class="fa fa-facebook" aria-hidden="true"></i>
										 		</a>
										 	</li>

										 	<li><a href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" title="Tweet this!">
										 		<i class="fa fa-twitter" aria-hidden="true"></i>
										 	</a></li>
										 	<li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" title="Share on LinkedIn"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
										 </ul>
										 
									</header>

									<?php the_content(); ?>

									<!--<?php custom_post_nav(); ?>-->

									<div class="button-wrapper">
										<a href="<?php echo esc_url(home_url('/')) ?>" class="call-btn tran-btn news-btn">Home</a>
										<a href="<?php echo get_post_type_archive_link( 'news' ); ?>" class="call-btn tran-btn news-btn">News</a>
									</div>
								</div>

								
							</section>
						</div>
					</article>
				</div>
			</div>

			<!--<div class="row news-row news-group">
				

  				<div class="col-xs-12 related-posts">
  					<h3 class="news-header black">Related news</h3>
  				</div>

  				<div class="col-xs-12 related-posts-bottom">
  					<?php $related = related_posts( get_the_ID(), -1 );
 
					if( $related->have_posts() ):
  				?>


  			<div class="related-news-rotator clearfix">

		  			<?php while( $related->have_posts() ): $related->the_post(); ?>
		  				 
		  				 	<div class="news-cell post-<?php echo ($xyz++%4); ?>">

		  				 		<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

		  				 		
		  				 			<article style="background-image: url('<?php echo $thumb['0'];?>')" class="news-box news-l">

					                  <div class="news-inside">
					                    <div class="news-cont">
					                      <h3 class="news-header"><?php the_title(); ?></h3>
					                      <p><?php the_excerpt_max_charlength(140); ?></p>
					                      <a href="<?php the_permalink(); ?>" class="call-btn tran-btn">Read more</a>
					                    </div>
					                  </div>
					                </article>
		  				 	
		  				 		
		  				 	</div>
		  				 
		            	<?php endwhile; ?>
            		</div>

  				</div>

      <?php endif;
			wp_reset_postdata(); ?>

			</div>-->


		</div>


	</section>
</div>


<?php get_footer(); ?>