<?php
  
  query_posts(array(
    'post_type' => 'page',
    'post__not_in' => array(98),
    'posts_per_page' => '-1',
    'order' => 'ASC',
    'orderby' => 'menu_order'
));

$tpl_parts = array(
  '5'=>'what-we-do',
  '7'=>'who-we-are',
  '9'=>'how-we-do-it',
  '11'=>'realising-a-return',
  '13'=>'who-we-do-it-for',
  '124'=>'slogan-banner',
  '15'=>'news-and-events',
  '17'=>'clients',
  '19'=>'testimonials',
  '21'=>'bottom-details'
);

?>

<?php get_header('home'); ?>

<div class="main-container">

  <?php while(have_posts()) : the_post(); ?>

    <section id="<?php echo $post->post_name; ?>" class="section">

        <?php if(array_key_exists($post->ID, $tpl_parts)) : ?>

            <?php get_part ($tpl_parts[$post->ID]); ?>

        <?php else : ?>
            <?php the_content(); ?>
        <?php endif; ?>

    </section>

<?php endwhile; ?>


</div>


<?php get_footer('home'); ?>